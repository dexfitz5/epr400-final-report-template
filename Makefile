##
##  Department of Electrical, Electronic and Computer Engineering.
##  EPR400/2 Final Report - makefile
##  Copyright (C) 2011-2019 University of Pretoria.
## 

SOURCE1=finalreport
PDFREADER=evince

# added to stop messy file structure
OUT=.build
FILES=*.log *.lof *.lot *.aux *.bbl *.blg *.toc *.acn *.glo *.ist *.acr *.alg *.glg *.gls *.glsdefs *.out 
MINTED=_minted-final-report/
# defualt make - compile just for small changes. 
all: $(SOURCE1).pdf

$(SOURCE1).pdf:  *.tex *.bib epr400.cls
	cd $(OUT); cp -r $(FILES) ../
	pdflatex -shell-escape -halt-on-error $(SOURCE1).tex

	mv $(FILES) $(OUT)
	cp -r $(MINTED) $(OUT)
	rm -r $(MINTED)
	$(PDFREADER) $(SOURCE1).pdf & echo $$! > evince.PID

# recompile everything
full: *.tex *.bib epr400.cls
	if [ -a evince.PID ]; then \
		kill -TERM $$(cat evince.PID) || true; \
	fi;
	pdflatex -shell-escape -halt-on-error $(SOURCE1).tex
	makeglossaries $(SOURCE1)
	bibtex $(SOURCE1)
	# needed for the bib unit of the proposal
	if [-a bul.aux]; then \
		bibtex   bu1 ;\
	fi
	pdflatex -shell-escape -halt-on-error $(SOURCE1).tex
	pdflatex -shell-escape -halt-on-error $(SOURCE1).tex

	mv $(FILES) $(OUT)
	cp -r $(MINTED) $(OUT)
	rm -r $(MINTED)
	$(PDFREADER) $(SOURCE1).pdf & echo $$! > evince.PID


#open the pdf in a viewer
view: $(SOURCE1).pdf
	$(PDFREADER) $(SOURCE1).pdf & echo $$! > evince.PID

#clean the working directory
clean:
	rm -f $(SOURCE1).pdf
	rm -r -f *.aux *.log *.bbl *.blg *.toc *.acn *.glo *.ist *.acr *.alg *.glg *.gls *.glsdefs *.out _minted-finalreport/ *.lof *.lot
	rm -f $(OUT) 2>/dev/null

outdir:
	mkdir -p $(OUT)
## End of File.