# EPR400 Final Report Template

The final report template for EPR400/402. Edited specifically for the students of the 2019 final year group.

## Disclaimer

This template is provided as is. Please read the study guide and the appendices available on <http://www2.ee.up.ac.za/~epr400/> for the latest contents, style and layout.

This template is provided by a third party and is free to use and edit. Please see the LICENCE for further details.

The original copyright belongs to Department of Electrical, Electronic and Computer Engineering at the University of Pretoria.

Copyright (C) 2011-2019 University of Pretoria.

## Forking this template

If you also want to use git for your report, I suggest that you read <https://help.github.com/en/articles/merging-an-upstream-repository-into-your-fork>

## Compilation

Install texlive-full on your operating system.

    sudo apt install texlive-full

To compile:

    make

or

    make full

## Technical Documentation

The technical documentation section is wrapped in an if-else statement, that is dependant on a boolean option. The boolean option is on Line 112 of finalreport.tex. Commenting out this line sets the option to false.

## Proposal Section

As per the new 2019 study guide, the final proposal must just be pasted in. Just use the code from 'proposal.tex'.

If your pdf builder freaks out and the fill-in blocks are empty, open the original pdf form, print to a new file, then include that file. Its the form, not the LaTeX.

The error in the command line is:

    *** BUG ***
    In pixman_region32_init_rect: Invalid rectangle passed
    Set a breakpoint on '_pixman_log_error' to debug

## Figures

Install inkscape on your operating system

    sudo apt install inkscape

Place svg figures in the figures directory

To import the svg figures into latex run

    make

in the figures directory, or run `make full`. 

Then include the `foo.pdf_tex` in your report by using `\input{figures/foo.pdf_tex}`

for example


    \begin{figure}[H]
        \label{fig:threadHierarchy}
        \centering
        \def\svgwidth{0.75\columnwidth} 
        \input{figures/threadHier.pdf_tex}
        \caption{CUDA thread hierarchy~\cite{nickolls_scalable_2008}}
    \end{figure}



<!-- Exporting svg images

```
    inkscape -D -z --file=image.svg --export-pdf=image.pdf --export-latex
``` -->